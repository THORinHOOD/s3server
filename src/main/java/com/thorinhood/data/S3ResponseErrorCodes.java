package com.thorinhood.data;

public class S3ResponseErrorCodes {
    public static final String INTERNAL_ERROR = "InternalError";
    public static final String BUCKET_ALREADY_OWNED_BY_YOU = "BucketAlreadyOwnedByYou";
    public static final String NO_SUCH_KEY = "NoSuchKey";
}
